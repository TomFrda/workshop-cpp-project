cmake_minimum_required(VERSION 3.17)

project(RType VERSION 1.0 LANGUAGES CXX)

if (POLICY CMP0072)
    set(OpenGL_GL_PREFERENCE GLVND)
endif()

###############################################################################
# CONFIG GAME ENGINE ECS MODULES
###############################################################################

SET(GAMEENGINE_USE_AREA_MODULE ON)
SET(GAMEENGINE_USE_TEXT_MODULE ON)
SET(GAMEENGINE_USE_IMAGE_MODULE ON)
SET(GAMEENGINE_USE_ANIMATION_MODULE ON)
SET(GAMEENGINE_USE_COLLISION_MODULE ON)
SET(GAMEENGINE_USE_AUDIO_MODULE ON)

###############################################################################
# BUILD GAME ENGINE
###############################################################################

add_subdirectory(GameEngine/)

###############################################################################
# BUILD RTYPE CLIENT
###############################################################################

add_subdirectory(Client/)

###############################################################################
# BUILD RTYPE SERVER
###############################################################################

add_subdirectory(Server/)

###############################################################################
# BUILD PONG
###############################################################################

add_subdirectory(Pong/)

###############################################################################
# BUILD RTYPE
###############################################################################

add_subdirectory(RType/)
