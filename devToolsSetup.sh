#!/bin/bash

set -eo pipefail

NoColor="\033[0m"
CyanColor="\033[0;96m"
GreenColor="\033[0;92m"

echo -e "${CyanColor}Installing git hooks...${NoColor}"
cp .ci/commit-msg .git/hooks/commit-msg
chmod +x .git/hooks/commit-msg
cp .ci/pre-push .git/hooks/pre-push
chmod +x .git/hooks/pre-push

echo -e "${GreenColor}All the developper tools have successfully been installed${NoColor}"
