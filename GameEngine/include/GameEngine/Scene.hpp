#pragma once

namespace ge {

class IScene {
   public:
    IScene() noexcept = default;
    IScene(IScene const& other) noexcept = default;
    IScene(IScene&& other) noexcept = default;
    virtual ~IScene() noexcept = default;

    IScene& operator=(IScene const& other) noexcept = default;
    IScene& operator=(IScene&& other) noexcept = default;

    virtual void update(float elapsedTime) = 0;
};

} // namespace ge
