/**
 * @file GameEngine.hpp
 * @brief Game engine using unique instance
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ECS/Core/Coordinator.hpp"
#include "EventsHandler.hpp"
#include "GameEngine/Window.hpp"
#include "ResourcesManager.hpp"
#include "Scene.hpp"
#include "Utils/Singleton.hpp"

#include <memory>

/**
 * @namespace ge
 * @brief Game engine related functions and objects
 */
namespace ge {

/**
 * @class GameEngine
 * @brief Game engine unique instance
 */
class GameEngine final : public utils::Singleton<GameEngine> {
   public:
    GameEngine() noexcept = delete;
    explicit GameEngine(token t) noexcept :
        Singleton(t) { }
    GameEngine(GameEngine const& other) noexcept = delete;
    GameEngine(GameEngine&& other) noexcept = delete;
    ~GameEngine() noexcept = default;

    GameEngine& operator=(GameEngine const& other) noexcept = delete;
    GameEngine& operator=(GameEngine&& other) noexcept = delete;

    /**
     * @brief Get the ECS coordinator
     *
     * @return A reference to the ECS coordinator
     */
    static ge::ecs::core::Coordinator& getCoordinator() noexcept;
    /**
     * @brief Get the graphic window
     *
     * @return A reference to the graphic window
     */
    static Window& getWindow() noexcept;
    /**
     * @brief Get the events handler
     *
     * @return A reference to the events handler
     */
    static EventsHandler& getEventsHandler() noexcept;
    /**
     * @brief Template a callback creation
     *
     * @tparam SceneType The scene type to use
     * @tparam CArgs The type of the arguments to use in the constructor of the scene
     */
    template <typename SceneType, typename... CArgs>
    /**
     * @brief Set the actual scene to use
     * @warning The scene type given as template argument must inherit from IScene
     *
     * @param cargs The arguments to capture and pass to the scene constructor
     *
     * @throw Anything the scene constructor could throw
     */
    static void setScene(CArgs&&... cargs)
    {
        auto& instance = getInstance();

        instance.scene.reset(nullptr);
        instance.scene = std::make_unique<SceneType>(std::forward<CArgs>(cargs)...);
    }
    /**
     * @brief Clear the scene used actually
     */
    static void clearScene() noexcept
    {
        getInstance().scene.reset(nullptr);
    }
    /**
     * @brief Start the main loop of the game
     *
     * @throw Anything the main loop could throw (ecs errors, resources errors, update function errors...)
     */
    static void start();
    /**
     * @brief Enable the functionality to pause the game by pressing the given keyboard key
     *
     * @param key The key to pause/unpause the game when is pressed/released
     */
    static void enableGamePause(KeyboardKeys key = KeyboardKeys::Enter) noexcept;
    /**
     * @brief Disable the functionality to pause the game by pressing a keyboard key
     */
    static void disableGamePause() noexcept;
    /**
     * @brief Enable the functionality to slow the game by pressing the given keyboard key
     *
     * @param key The key to slow/unslow the game when is pressed/released
     * @param slowingValue The value of the game slowing, the higher this value is, the slower the game will be, passing 2 as slowingValue will make the game 2 times slower
     */
    static void enableGameSlow(KeyboardKeys key = KeyboardKeys::Space, float slowingValue = 5) noexcept;
    /**
     * @brief Disable the functionality to slow the game by pressing a keyboard key
     */
    static void disableGameSlow() noexcept;
    /**
     * @brief Enable or disable the functionality to display displayed frame per seconds
     *
     * @param show If the FPS should be shown or not
     */
    static void showFPS(bool show = true) noexcept;

   private:
    /**
     * @brief Handle function for pause key callback
     */
    void handlePause() noexcept;
    /**
     * @brief Handle function for slow key callback
     */
    void handleSlow() noexcept;

    /**
     * @var pauseEnabled
     * @brief If the functionality to pause the game is enabled or not
     */
    bool pauseEnabled = false;
    /**
     * @var pauseKey
     * @brief The keyboard key that is used to pause and unpause the game
     */
    KeyboardKeys pauseKey{};
    /**
     * @var slowingEnabled
     * @brief If the functionality to slow the game is enabled or not
     */
    bool slowingEnabled = false;
    /**
     * @var slowingValue
     * @brief The value of the slowing, the higher this value is, the slower the game will get
     */
    float slowingValue = 0;
    /**
     * @var slowKey
     * @brief The keyboard key that is used to slow and unslow the game
     */
    KeyboardKeys slowKey{};
    /**
     * @var fpsDisplayEnabled
     * @brief If the functionality to display FPS is enabled or not
     */
    bool fpsDisplayEnabled = false;
    /**
     * @var inPause
     * @brief If game should be paused or not
     */
    bool inPause = false;
    /**
     * @var inSlow
     * @brief If game should be slowed or not
     */
    bool inSlow = false;
    /**
     * @var scene
     * @brief The actual scene
     */
    std::unique_ptr<IScene> scene;
    /**
     * @var coordinator
     * @brief The ECS coordinator
     */
    ge::ecs::core::Coordinator coordinator;
    /**
     * @var window
     * @brief The graphic window
     */
    Window window;
    /**
     * @var eventsHandler
     * @brief The window events handler
     */
    EventsHandler eventsHandler;
    /**
     * @var updateCallback
     * @brief The callback to call to update game
     */
    utils::Callback<void(float)> updateCallback;
};

/**
 * @var getCoordinator
 * @brief Alias to Game engine ECS coordinator
 */
constexpr auto getCoordinator = GameEngine::getCoordinator;
/**
 * @var getWindow
 * @brief Alias to Game engine window
 */
constexpr auto getWindow = GameEngine::getWindow;
/**
 * @var getEventsHandler
 * @brief Alias to Game engine events handler
 */
constexpr auto getEventsHandler = GameEngine::getEventsHandler;

} // namespace ge
