/**
 * @file Utils.hpp
 * @brief Utilitary functions that can do anything
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/System/Vector2.hpp"
#include "Vector.hpp"

/**
 * @namespace utils
 * @brief Utilitary functions and classes
 */
namespace ge::utils {

/*
 * @brief Get the rotation angle of the given vector according to x axis
 *
 * @return The rotation angle in degrees
 *
 * @param vector The vector to get angle for
 */
float getRotationAngleFromX(const utils::Vector& vector) noexcept;

/**
 * @brief Templatable structure of types
 *
 * @tparam T The type of the four stored vars
 */
template <typename T>
/**
 * @struct Rect
 * @brief Store four values (one for each rectangle side)
 */
struct Rect {
    T left = 0;
    T top = 0;
    T bottom = 0;
    T right = 0;
};

} // namespace ge::utils
