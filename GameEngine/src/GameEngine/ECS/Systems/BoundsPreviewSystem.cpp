#ifdef USE_COLLISION_MODULE

#include "GameEngine/ECS/Systems/BoundsPreviewSystem.hpp"

#include "GameEngine/ECS/Components/Bounds.hpp"
#include "GameEngine/ECS/Components/Transform.hpp"

ge::ecs::system::BoundsPreviewSystem::BoundsPreviewSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::BoundsPreviewSystem::update(float elapsedTime)
{
    for (auto const& entity : this->entities) {
        auto& transform = this->coordinator.getComponent<ecs::component::Transform>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        bounds.transform = (transform.movement * elapsedTime) / float{1000};
    }
}

#endif
