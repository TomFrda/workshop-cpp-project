#ifdef USE_COLLISION_MODULE

#include "GameEngine/ECS/Systems/CollisionSystem.hpp"

#include "GameEngine/ECS/Components/Bounds.hpp"
#include "GameEngine/ECS/Components/Collider.hpp"
#include "GameEngine/Utils/Utils.hpp"

#include <algorithm>
#include <optional>
#include <unordered_map>

ge::ecs::system::CollisionSystem::CollisionSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

template <typename T>
static ge::utils::Rect<T> getCollidingFaces(sf::Rect<T> const& r1, sf::Rect<T> const& r2, sf::Rect<T> const& intersection) noexcept
{
    ge::utils::Rect<T> collidingFaces;

    if (r2.top < r1.top and r1.top < r2.top + r2.height) {
        collidingFaces.top = intersection.height;
    }
    if (r2.top < r1.top + r1.height and r1.top + r1.height < r2.top + r2.height) {
        collidingFaces.bottom = intersection.height;
    }
    if (r2.left < r1.left and r1.left < r2.left + r2.width) {
        collidingFaces.left = intersection.width;
    }
    if (r2.left < r1.left + r1.width and r1.left + r1.width < r2.left + r2.width) {
        collidingFaces.right = intersection.width;
    }

    return std::move(collidingFaces);
}

void ge::ecs::system::CollisionSystem::update(std::size_t i, std::size_t index2, std::set<std::size_t> doneY)
{
    std::set<std::size_t> doneI;

    bool firstLoop = true;
    for (; i < this->entities.size(); i++) {
        auto bounds1 = this->coordinator.getComponent<ecs::component::Bounds>(this->entities[i]);
        auto& collider1 = this->coordinator.getComponent<ecs::component::Collider>(this->entities[i]);
        bounds1.bounds.top += bounds1.transform.y;
        bounds1.bounds.left += bounds1.transform.x;

        if (not firstLoop) {
            doneY.clear();
        }
        for (std::size_t y = (firstLoop ? index2 : i + 1); y < this->entities.size(); y++) {
            doneY.insert(this->entities[y]);
            auto bounds2 = this->coordinator.getComponent<ecs::component::Bounds>(this->entities[y]);
            auto& collider2 = this->coordinator.getComponent<ecs::component::Collider>(this->entities[y]);
            bounds2.bounds.top += bounds2.transform.y;
            bounds2.bounds.left += bounds2.transform.x;

            auto intersection = bounds1.bounds.findIntersection(bounds2.bounds);
            if (intersection.has_value()) {
                ge::utils::Rect<float> collidingFaces1 = getCollidingFaces(bounds1.bounds, bounds2.bounds, intersection.value());
                ge::utils::Rect<float> collidingFaces2 = getCollidingFaces(bounds2.bounds, bounds1.bounds, intersection.value());
                auto e1 = ge::ecs::core::Entity(this->entities[i]);
                auto e2 = ge::ecs::core::Entity(this->entities[y]);
                collider1.runCallback(e2, collider2, collidingFaces1);
                collider2.runCallback(e1, collider1, collidingFaces2);
                std::size_t nextI = -1;
                std::size_t nextY = -1;
                std::set<std::size_t> newDoneY;
                if (this->entities.size() > i and this->entities[i] == e1) {
                    nextI = i;
                } else {
                    for (std::size_t j = 0; j < this->entities.size(); j++) {
                        if (not doneI.contains(this->entities[j])) {
                            nextI = j;
                            break;
                        }
                    }
                }
                if (nextI == -1 or nextI >= this->entities.size()) {
                    return;
                }
                if (this->entities[nextI] == e1) {
                    for (std::size_t j = nextI + 1; j < this->entities.size(); j++) {
                        if (not doneY.contains(this->entities[j])) {
                            newDoneY = std::move(doneY);
                            nextY = (this->entities[j] == e2 ? j + 1 : j);
                            break;
                        }
                    }
                    if (nextY == -1) {
                        nextI++;
                        nextY = nextI + 1;
                    }
                } else {
                    nextY = nextI + 1;
                }
                if (nextY == -1 or nextY >= this->entities.size()) {
                    return;
                }
                return this->update(nextI, nextY, newDoneY);
            }
        }
        doneI.insert(this->entities[i]);
        firstLoop = false;
    }
}

#endif
