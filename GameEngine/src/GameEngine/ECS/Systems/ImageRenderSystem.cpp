#ifdef USE_IMAGE_MODULE

#include "GameEngine/ECS/Systems/ImageRenderSystem.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/RenderableImage.hpp"

ge::ecs::system::ImageRenderSystem::ImageRenderSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::ImageRenderSystem::update(Window& window)
{
    for (auto const& entity : this->entities) {
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);

        image.setPosition(attributes.position);
        image.setScale(attributes.scale);
        image.setRotation(attributes.angle);

        if (not image.isHidden()) {
            image.draw(window.window);
        }
    }
}

#endif
