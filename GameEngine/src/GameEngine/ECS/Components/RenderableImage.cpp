#ifdef USE_IMAGE_MODULE

#include "GameEngine/ECS/Components/RenderableImage.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/ECSErrors.hpp"
#include "GameEngine/ResourcesManager.hpp"
#include "GameEngine/Utils/Vector.hpp"
#include "fmt/core.h"

ge::ecs::component::RenderableImage::RenderableImage(std::string_view texturePath, sf::IntRect area, bool centered)
{
    this->setTexture(texturePath, area, centered);
}

ge::ecs::component::RenderableImage::RenderableImage(std::string_view texturePath, utils::Vector area, bool centered) :
    RenderableImage(texturePath, sf::IntRect({0, 0}, area), centered)
{
}

ge::ecs::component::RenderableImage::RenderableImage(RenderableImage&& other) noexcept :
    sprite(std::move(other.sprite)), hidden(other.hidden)
{
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setTexture(std::string_view texturePath, sf::IntRect area, bool centered)
{
    try {
        this->sprite.setTexture(ge::ResourcesManager::getTexture(std::string(texturePath)));
        if (area.width > 0 or area.height > 0) {
            this->sprite.setTextureRect(area);
        }
        if (centered) {
            auto size = this->sprite.getLocalBounds();
            this->sprite.setOrigin({size.width / 2, size.height / 2});
        } else {
            this->sprite.setOrigin({0, 0});
        }
    } catch (ge::error::ResourceError const& e) {
        throw error::ComponentError("RenderableImage", fmt::format("Failed to create graphic object: '{}'", e.what()));
    }

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setTexture(std::string_view texturePath, utils::Vector area, bool centered)
{
    this->setTexture(texturePath, {{0, 0}, area}, centered);

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setColor(sf::Color color) noexcept
{
    this->sprite.setColor(color);

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setPosition(utils::Vector position) noexcept
{
    this->sprite.setPosition(position);

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setScale(utils::Vector scale) noexcept
{
    this->sprite.setScale(scale);

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setRotation(float angle) noexcept
{
    this->sprite.setRotation(sf::degrees(angle));

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::draw(sf::RenderWindow& window) noexcept
{
    window.draw(this->sprite);

    return *this;
}

ge::ecs::component::RenderableImage& ge::ecs::component::RenderableImage::setArea(sf::IntRect newArea) noexcept
{
    this->sprite.setTextureRect(newArea);

    return *this;
}

sf::FloatRect ge::ecs::component::RenderableImage::getBounds() const noexcept
{
    return this->sprite.getGlobalBounds();
}

sf::Color ge::ecs::component::RenderableImage::getColor() const noexcept
{
    return this->sprite.getColor();
}

void ge::ecs::component::RenderableImage::hide(bool hide) noexcept
{
    this->hidden = hide;
}

bool ge::ecs::component::RenderableImage::isHidden() const noexcept
{
    return this->hidden;
}

#endif
