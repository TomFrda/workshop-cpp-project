/**
 * @file SystemsManager.hpp
 * @brief A manager for ECS systems
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ASystem.hpp"
#include "Component.hpp"
#include "ECS/ECSErrors.hpp"
#include "fmt/core.h"

#include <memory>
#include <typeindex>
#include <unordered_map>
#include <utility>

namespace ecs::core {

class Coordinator;

/**
 * @class SystemsManager
 * @brief A class to manage ECS systems
 */
class SystemsManager {
   public:
    SystemsManager() noexcept = default;
    SystemsManager(SystemsManager const& other) noexcept = delete;
    SystemsManager(SystemsManager&& other) noexcept = default;
    ~SystemsManager() noexcept = default;

    SystemsManager& operator=(SystemsManager const& other) noexcept = delete;
    SystemsManager& operator=(SystemsManager&& other) noexcept = default;

    /**
     * @brief Templated system action
     *
     * @tparam T The type of the system
     */
    template <typename T>
    /**
     * @brief Register a new system in the ECS
     *
     * @warning This function should be called only one time for the same system
     *
     * @return A reference to the newly constructed system so the systems updator functions can be called in the code
     *
     * @throw error::ECSError thrown if this function is called more than one time for the same system type
     */
    T& registerSystem(Coordinator& coordinator)
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (this->systemsSignatures.contains(type)) {
            throw error::ECSError(fmt::format("Registering same system({}) two times", typeid(T).name()));
        }

        this->systemsSignatures.insert({type, {}});
        this->systems.insert({type, std::make_unique<T>(coordinator)});
        return dynamic_cast<T&>(*(this->systems.at(type)));
    }
    /**
     * @brief Templated system action
     *
     * @tparam T The type of the system
     */
    template <typename T>
    /**
     * @brief Set the signature of the system
     *
     * @param signature The signature to associate to the system
     *
     * @throws error::ECSError thrown if the system has not been registered in the ECS
     */
    void setSystemSignature(ComponentSignature const& signature)
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (not this->systemsSignatures.contains(type)) {
            throw error::ECSError(fmt::format("System({}) not registered before use", typeid(T).name()));
        }

        this->systemsSignatures[type] = signature;
    }
    /**
     * @brief Templated system action
     *
     * @tparam T The type of the system
     */
    template <typename T>
    void setSystemSignature(ComponentSignature&& signature)
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (not this->systemsSignatures.contains(type)) {
            throw error::ECSError(fmt::format("System({}) not registered before use", typeid(T).name()));
        }

        this->systemsSignatures[type] = std::move(signature);
    }
    /**
     * @brief Signal that an entity signature have changed
     *
     * @param entity The entity that has it signature changed
     * @param newEntitySignature The new signature of the given entity
     */
    void entitySignatureChanged(Entity& entity, ComponentSignature const& newEntitySignature) noexcept;
    /**
     * @brief Signal that the entity have been destroyed
     *
     * @param entity The entity that got destroyed
     */
    void entityDestroyed(Entity& entity) noexcept;

   private:
    /**
     * @brief The list of systems signatures link to their system type
     */
    std::unordered_map<std::type_index, ComponentSignature> systemsSignatures;
    /**
     * @brief List of systems
     */
    std::unordered_map<std::type_index, std::unique_ptr<ASystem>> systems;
};

} // namespace ecs::core
