/**
 * @file AudioError.hpp
 * @brief Audio realted error declaration
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <exception>
#include <string>

/**
 * @namespace audio::error
 * @brief Error music related
 */
namespace audio::error {

/**
 * @class AudioError
 * @brief Error thrown when an audio related object failed to do some operation
 */
class AudioError : public std::exception {
   public:
    AudioError() noexcept = delete;
    /**
     * @brief Generate an object to throw when an error occurs in audio
     *
     * @param message A message that describe the error
     */
    explicit AudioError(std::string const& message);
    AudioError(AudioError const& other) = default;
    AudioError(AudioError&& other) = default;
    ~AudioError() override = default;

    AudioError& operator=(AudioError const& other) = default;
    AudioError& operator=(AudioError&& other) = default;

    /**
     * @brief Get a description of the error
     *
     * @return The error message defined in constructor
     */
    const char* what() const noexcept override;

   protected:
    /**
     * @var message
     * @brief Store the message gived in the constructor that describe the error
     */
    std::string message;
};

/**
 * @class MusicError
 * @brief Error thrown when a Music object failed to do some operation
 */
class MusicError final : public AudioError {
   public:
    MusicError() noexcept = delete;
    /**
     * @brief Generate an object to throw when an error occurs in a Music
     *
     * @param message A message that describe the error
     */
    explicit MusicError(std::string const& message);
    MusicError(MusicError const& other) = default;
    MusicError(MusicError&& other) = default;
    ~MusicError() final = default;

    MusicError& operator=(MusicError const& other) = default;
    MusicError& operator=(MusicError&& other) = default;

    /**
     * @brief Get a description of the error
     *
     * @return The error message defined in constructor
     */
    const char* what() const noexcept final;
};

/**
 * @class SoundError
 * @brief Error thrown when a Sound object failed to do some operation
 */
class SoundError final : public AudioError {
   public:
    SoundError() noexcept = delete;
    /**
     * @brief Generate an object to throw when an error occurs in a Sound
     *
     * @param message A message that describe the error
     */
    explicit SoundError(std::string const& message);
    SoundError(SoundError const& other) = default;
    SoundError(SoundError&& other) = default;
    ~SoundError() final = default;

    SoundError& operator=(SoundError const& other) = default;
    SoundError& operator=(SoundError&& other) = default;

    /**
     * @brief Get a description of the error
     *
     * @return The error message defined in constructor
     */
    const char* what() const noexcept final;
};

} // namespace audio::error
