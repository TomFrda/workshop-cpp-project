#include "Scenes/GameScene.hpp"

#include "GameEngine/GameEngine.hpp"
#include "Scenes/EndScene.hpp"

scene::GameScene::GameScene() :
    parallaxManager(ge::getCoordinator(), this->filepaths, 100, 500), borderWalls(ge::getCoordinator()), player(ge::getCoordinator(), this->bulletManager, {ge::getWindow().getSize().x / 20, ge::getWindow().getSize().y / 2}), enemyManager(this->explosionManager, this->bulletManager, this->player), enemyCreator(ge::getCoordinator(), this->enemyManager, this->player)
{
}

void scene::GameScene::update(float elapsedTime)
{
    this->enemyCreator.update(elapsedTime);
    this->bulletManager.updateBullets(elapsedTime);
    if (not this->player.isAlive()) {
        ge::GameEngine::setScene<scene::LoseScene>(this->player.getPlayersKill());
    }
}
