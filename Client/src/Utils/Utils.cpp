#include "Utils/Utils.hpp"

#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Area.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Collider.hpp"
#include "ECS/Components/Parallax.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"

#include <cmath>
#include <iostream>
#include <numbers>

void utils::registerComponents(ecs::core::Coordinator& coordinator)
{
    coordinator.registerComponent<ecs::component::RenderableImage>();
    coordinator.registerComponent<ecs::component::Attributes>();
    coordinator.registerComponent<ecs::component::Transform>();
    coordinator.registerComponent<ecs::component::Bounds>();
    coordinator.registerComponent<ecs::component::Animation>();
    coordinator.registerComponent<ecs::component::Collider>();
    coordinator.registerComponent<ecs::component::Parallax>();
    coordinator.registerComponent<ecs::component::Area>();
}

static bool printUsage(const std::string& path) noexcept
{
    std::cout << "USAGE: " << path << std::endl;
    return true;
}

bool utils::checkUsage(int ac, const char* av[]) noexcept
{
    for (int i = 0; i < ac; i++) {
        std::string arg = av[i];
        if (arg == "-h" or arg == "--help") {
            return printUsage(av[0]);
        }
    }
    return false;
}

void utils::displayFPS(float dt) noexcept
{
    std::cout << "FPS: " << float{1000} / dt << std::endl;
}

float utils::getRotationAngleFromX(const sf::Vector2f& vector) noexcept
{
    return std::atan2(vector.y, vector.x) * (180 / std::numbers::pi);
}
