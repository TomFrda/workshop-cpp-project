#!/bin/bash

set -eo pipefail

NoColor="\033[0m"
CyanColor="\033[0;36m"
RedColor="\033[0;91m"

compileArgs=""
buildArgs=""
compiler="clang++"

rtypeClientBinName=r-type_client
rtypeClientUTBinName=unit_tests_client
rtypeServerBinName=r-type_server
rtypeServerUTBinName=unit_tests_server
rtypeBinName=rtype
otherBinName=pong

function generatecc {
    compileArgs="$compileArgs -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
}

function compiledefault {
    compileArgs="$compileArgs -DCMAKE_BUILD_TYPE=Default"
}

function compiledebug {
    compiler="g++"
    compileArgs="$compileArgs -DCMAKE_BUILD_TYPE=Debug"
}

function compilerelease {
    compileArgs="$compileArgs -DCMAKE_BUILD_TYPE=Release"
}

function compiletests {
    compileArgs="$compileArgs -DTESTING=ON"
}

function compileserver {
    compiling="server"
    builddir="./build/server.d"
    buildArgs="$buildArgs --target $rtypeServerBinName"
}

function compileclient {
    compiling="client"
    builddir="./build/client.d"
    buildArgs="$buildArgs --target $rtypeClientBinName"
}

function compilertype {
    compiling="rtype"
    builddir="./build/rtype.d"
    buildArgs="$buildArgs --target $rtypeBinName"
}

function compilepong {
    compiling="pong"
    builddir="./build/pong.d"
    buildArgs="$buildArgs --target $otherBinName"
}

function compileboth {
    compiling="both"
    builddir="./build/both.d"
    buildArgs="$buildArgs --target $rtypeClientBinName --target $rtypeServerBinName"
}

function clean {
    if [[ "$compiling" == "client" ]] || [[ "$compiling" == "both" ]]; then
        rm -f $rtypeClientBinName
        rm -f $rtypeClientUTBinName
        rm -f build/$rtypeClientBinName
        rm -f build/$rtypeClientUTBinName
    fi
    if [[ "$compiling" == "server" ]] || [[ "$compiling" == "both" ]]; then
        rm -f $rtypeServerBinName
        rm -f $rtypeServerUTBinName
        rm -f build/$rtypeServerBinName
        rm -f build/$rtypeServerUTBinName
    fi
    if [[ "$compiling" == "pong" ]]; then
        rm -f $otherBinName
        rm -f build/$otherBinName
    fi
    if [[ "$compiling" == "rtype" ]]; then
        rm -f $rtypeBinName
        rm -f build/$rtypeBinName
    fi
}

function fclean {
    clean
    rm -rf $builddir
}

function aclean {
    clean
    rm -rf build
}

function compile {
    mkdir -p $builddir && cd $builddir

    cmake ../../ -G "Ninja" -Wno-dev -Wno-deprecated -DCMAKE_CXX_COMPILER=$compiler $compileArgs
    cmake --build . $buildArgs

    cd ../..

    if [ -f build/$rtypeClientBinName ]; then
        cp build/$rtypeClientBinName .
    fi
    if [ -f build/$rtypeClientUTBinName ]; then
        cp build/$rtypeClientUTBinName .
    fi
    if [ -f build/$rtypeServerBinName ]; then
        cp build/$rtypeServerBinName .
    fi
    if [ -f build/$rtypeServerUTBinName ]; then
        cp build/$rtypeServerUTBinName .
    fi
    if [ -f build/$otherBinName ]; then
        cp build/$otherBinName .
    fi
    if [ -f build/$rtypeBinName ]; then
        cp build/$rtypeBinName .
    fi
}

function copycc {
    if [ -f $builddir/compile_commands.json ]; then
        cp $builddir/compile_commands.json .
    fi
}

function launchtest {
    if [ -f "$1" ]; then
        ./$1
    else
        echo -e "${RedColor}Could not file test file '$1'${NoColor}"
    fi
}

function informlaunchtests {
    echo -e "${CyanColor}You can start the tests by executing './$rtypeClientUTBinName' and './$rtypeServerUTBinName' files${NoColor}"
}

scriptargs=" $* "

if [[ "$scriptargs" == *" -h "* ]] || [[ "$scriptargs" == *" --help "* ]] || [[ "$scriptargs" == *" help "* ]] || [[ "$scriptargs" == *" h "* ]]; then
    echo -e "Usage: $0 [server|client] [Options]\n"
    echo "The first argument is optional"
    echo    "  none      Compile RType server AND RType client."
    echo    "  pong      Compile pong."
    echo    "  rtype     Compile rtype solo mode."
    echo    "  client    Compile only RType server."
    echo -e "  server    Compile only RType client.\n"
    echo "Options(optional):"
    echo "  re        Recompile the project from scratch. Implicitly use 'fclean'."
    echo "  cc        Generate a 'compile_commands' file"
    echo "  debug     Recompile the project in debug mode. Implicitly use 'fclean'."
    echo "  release   Recompile the project in debug mode. Implicitly use 'fclean'."
    echo "  test      Launch tests for the project. Implicitly use 're'."
    echo "  clean     Remove all post-compilation files (binaries...). Does not compile."
    echo "  fclean    Remove all post-compilation and compilation files (binaries, build dir...). Does not compile."
    echo "  aclean    Remove all post-compilation and compilation files (binaries, build dir...) for all mode (client only, server only and both). Does not compile."
    exit 0
fi

set +eo pipefail
cmakePath=$( which cmake )
if [ ! $? -eq 0 ]; then
    echo -e "\n${RedColor}Please install cmake first${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using cmake at $cmakePath${NoColor}"
fi
ninjaPath=$( which ninja )
if [ ! $? -eq 0 ]; then
    echo -e "\n${RedColor}Please install ninja first${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using ninja at $ninjaPath${NoColor}\n"
fi
set -eo pipefail

if [[ "$1" == "server" ]] || [[ "$1" == "Server" ]] || [[ "$1" == "s" ]] || [[ "$1" == "S" ]]; then
    echo -e "\n${CyanColor}/!\\ Compiling only Server${NoColor}\n\n"
    compileserver
elif [[ "$1" == "client" ]] || [[ "$1" == "Client" ]] || [[ "$1" == "c" ]] || [[ "$1" == "C" ]]; then
    echo -e "\n${CyanColor}/!\\ Compiling only Client${NoColor}\n\n"
    compileclient
elif [[ "$1" == "pong" ]] || [[ "$1" == "Pong" ]] || [[ "$1" == "p" ]] || [[ "$1" == "P" ]]; then
    echo -e "\n${CyanColor}/!\\ Compiling only pong${NoColor}\n\n"
    compilepong
elif [[ "$1" == "rtype" ]] || [[ "$1" == "RType" ]] || [[ "$1" == "r" ]] || [[ "$1" == "R" ]]; then
    echo -e "\n${CyanColor}/!\\ Compiling only rtype solo mode${NoColor}\n\n"
    compilertype
else
    echo -e "\n${CyanColor}/!\\ Compiling both Server and Client${NoColor}\n\n"
    compileboth
fi

if [[ "$scriptargs" == *" cc "* ]] ; then
    echo -e "\n${CyanColor}Creating compile_commands.json file${NoColor}"
    generatecc
fi

if [[ "$scriptargs" == *" clean "* ]] ; then
    clean
elif [[ "$scriptargs" == *" fclean "* ]] ; then
    fclean
elif [[ "$scriptargs" == *" aclean "* ]] ; then
    aclean
else
    if [[ "$scriptargs" == *" re "* ]] || [[ "$scriptargs" == *" test "* ]] ; then
        echo -e "\n${CyanColor}Recompiling all the project${NoColor}"
        fclean
    fi

    if [[ "$scriptargs" == *" debug "* ]] ; then
        echo -e "\n${CyanColor}Compiling project in debug mode${NoColor}"
        compiledebug
    elif [[ "$scriptargs" == *" release "* ]] ; then
        echo -e "\n${CyanColor}Compiling project in release mode${NoColor}"
        compilerelease
    elif [[ "$scriptargs" == *" test "* ]] ; then
        echo -e "\n${CyanColor}Compiling project tests${NoColor}"
        compiletests
    else
        compiledefault
    fi

    compile
    copycc

    if [[ "$scriptargs" == *" test "* ]] ; then
        informlaunchtests
    fi
fi
