cmake_minimum_required(VERSION 3.21)

if (NOT sfml_FOUND)

    INCLUDE(FetchContent)

    FetchContent_Declare(sfml GIT_REPOSITORY https://github.com/SFML/SFML.git GIT_TAG fd5c358c98833c46264fe239a957a696dd5e1758)
    FetchContent_MakeAvailable(sfml)

    SET(BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

    set(sfml_FOUND TRUE)
endif()
